
const Score = (props) => {
    return (
        <div className="aaaaa">
            <h1> {props.children} </h1>
            <div className="points">
                <h2> {props.playerWins} </h2>
                <h2> {props.tieGame} </h2>
                <h2> {props.computerWins} </h2>
            </div>
            <div className="players">
                <p>Jogador</p>
                <p>Empates</p>
                <p>Computador</p>
            </div>
            <div>
                <div className="computerChoice">
                    {props.computer === 1 ? <p>Computador "escolheu": <i class="far fa-hand-rock"></i></p>
                    : 
                    props.computer === 2 ? <p>Computador "escolheu": <i class="far fa-hand-paper"></i></p>
                    : 
                    props.computer === 3 ? <p>Computador "escolheu": <i class="far fa-hand-scissors"></i></p>
                    : ""}
                </div>
            </div>
        </div>
    )
}

export default Score