import { useEffect, useState } from 'react';
import './App.css';
import Score from './components/Score';

const App = () => {
  
  const [player, setPlayer] = useState(0)
  const [computer, setComputer] = useState(0)
  const [playerWins, setPlayerWins] = useState(0)
  const [computerWins, setComputerWins] = useState(0)
  const [tieGame, setTieGame] = useState(0)

  function computerTurn() {
    const turn = Math.random()
    switch (true) {
        
      case turn <= 0.33:
          setComputer(1)
      break;
      
      case (turn > 0.33 && turn <= 0.66):
          setComputer(2)
      break;

      case (turn > 0.66):
          setComputer(3)
      break;
      default: return
    }
  }

  function checkWinner() {
    
    switch (true) {

      case ((player === 1) && (computer === 1)):
        setTieGame(tieGame + 1)
      break
      
      case ((player === 1 ) && (computer === 2)):
        setComputerWins(computerWins + 1)
      break

      case ((player === 1) && (computer === 3)):
        setPlayerWins(playerWins + 1)
      break

      case ((player === 2) && (computer === 2)):
        setTieGame(tieGame + 1)
      break

      case ((player === 2) && (computer === 3)):
        setComputerWins(computerWins + 1)
      break

      case ((player === 2) && (computer === 1)):
        setPlayerWins(playerWins + 1)
      break

      case ((player === 3) && (computer === 3)):
        setTieGame(tieGame + 1)
      break

      case ((player === 3) && (computer === 1)):
        setComputerWins(computerWins + 1)
      break

      case ((player === 3) && (computer === 2)):
        setPlayerWins(playerWins + 1)
      break

      default: 
    }
  }

  useEffect(() => {
    checkWinner()
  },[player, computer])
  
  return (
    <div className="App">
      <Score playerWins={playerWins} computerWins={computerWins} tieGame={tieGame} computer={computer}> Placar </Score>    
      <section className="buttons">
        <button onClick={() => {setPlayer(1); computerTurn()}} className={"btn"}><i class="far fa-hand-rock"></i></button>
        <button onClick={() => {setPlayer(2); computerTurn()}} className={"btn"}><i class="far fa-hand-paper"></i></button>
        <button onClick={() => {setPlayer(3); computerTurn()}} className={"btn"}><i class="far fa-hand-scissors"></i></button>
      </section>
      
    </div>
  );
}

export default App;
